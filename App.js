import React from 'react';
import Setup from './src/boot/Setup.js';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import rootReducers from './src/redux/reducers';

const store = createStore(rootReducers);

const App = () => {
  return (
    <Provider store={store}>
      <Setup />
    </Provider>
  );
}

export default App;
