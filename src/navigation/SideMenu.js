import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Image, TouchableHighlight, TouchableOpacity } from 'react-native';
import { ListItem, Text } from 'native-base'
import { NavigationActions } from 'react-navigation';
import { ScrollView, View } from 'react-native';

class SideMenu extends Component {
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

  render () {
    const { navigate } = this.props.navigation;

    return(
      <ScrollView>
        <TouchableOpacity key="close" onPress={() => navigate('DrawerClose')} style={styles.closeWrap}>
          <Image resizeMode="contain" source={require('../assets/menu/ic_close_xhdpi.png')} style={styles.closeIcon}/>
        </TouchableOpacity>
        <TouchableHighlight key="read" onPress={this.navigateToScreen('Content')} underlayColor='#ededed' style={styles.navItemWrap}>
          <Text style={styles.textWrapper}>
            <Image resizeMode="contain" source={require('../assets/menu/ic_read.png')} style={styles.imgStyle}/>{' '}{' '}
            <Text style={styles.navItem}>
              ԿԱՐԴԱԼ
            </Text>
          </Text>
        </TouchableHighlight>
        <TouchableHighlight key="game" onPress={this.navigateToScreen('Game')} underlayColor='#ededed' style={styles.navItemWrap}>
          <Text style={styles.textWrapper}>
          <Image resizeMode="contain" source={require('../assets/menu/ic_play.png')} style={styles.imgStyle}/>{' '}{' '}
            <Text style={styles.navItem}>
              ԽԱՂԱԼ
            </Text>
          </Text>
        </TouchableHighlight>
        <TouchableHighlight key="about" onPress={this.navigateToScreen('About')} underlayColor='#ededed' style={styles.navItemWrap}>
          <Text style={styles.textWrapper}>
            <Image resizeMode="contain" source={require('../assets/menu/ic_about.png')} style={styles.imgStyle}/>{' '}{' '}
            <Text style={styles.navItem}>
              ՄԵՐ ՄԱՍԻՆ
            </Text>
          </Text>
        </TouchableHighlight>
        <TouchableHighlight key="readinst" onPress={this.navigateToScreen('ReadInstructions')} underlayColor='#ededed' style={styles.navItemWrap}>
          <Text style={styles.textWrapper}>
            <Image resizeMode="contain" source={require('../assets/menu/ic_how_to_read.png')} style={styles.imgStyle}/>{' '}{' '}
            <Text style={styles.navItem}>
              ԿԱՐԴԱԼՈՒ ՄԱՍԻՆ
            </Text>
          </Text>
        </TouchableHighlight>
        <TouchableHighlight key="gameinst" onPress={this.navigateToScreen('GameInstructions')} underlayColor='#ededed' style={styles.navItemWrap}>
          <Text style={styles.textWrapper}>
            <Image resizeMode="contain" source={require('../assets/menu/ic_how_to_play.png')} style={styles.imgStyle}/>{' '}{' '}
            <Text style={styles.navItem}>
              ԽԱՂԱԼՈՒ ՄԱՍԻՆ
            </Text>
          </Text>
        </TouchableHighlight>
      </ScrollView>
    )
  }
}

const styles = {
  closeWrap: {
    width: 20,
    marginTop: 20,
    marginBottom: 50,
    marginLeft: 260,
    justifyContent: 'flex-end'
  },
  closeIcon: {
    height: 20,
    width: 20
  },
  navItemWrap: {
    paddingStart: 20,
    padding: 10
  },
  imgStyle: {
    width: 50,
    height: 50,
    resizeMode: 'contain'
  },
  textWrapper: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  navItem: {
    fontFamily: 'DejavuSans_bold',
    color: '#5e5f5e',
    marginBottom: 5
  },
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};

export default SideMenu;
