import React from 'react';
import { DrawerNavigator, TabNavigator, StackNavigator } from 'react-navigation';
import ListMain from '../components/content/list/ListMain';
import SingleMain from '../components/content/single/SingleMain';
import InitialChoice from '../components/welcome/InitialChoice';
import GameOverview from '../components/game/GameOverview';
import AboutMain from '../components/about/AboutMain';
import GameInstructions from '../components/instructions/GameInstructions';
import ReadInstructions from '../components/instructions/ReadInstructions';
import StartLevel from '../components/game/levels/StartLevel';
import SideMenu from './SideMenu';
import { Image } from 'react-native';

export const HomeTabs = TabNavigator({
  Male: { screen: ListMain },
  Female: { screen: ListMain }
},
{
  navigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, tintColor }) => {
      const { routeName } = navigation.state;
      let iconName;
      if (routeName === 'Male') {
        iconName = require('../assets/content/male.png');
      } else if (routeName === 'Female') {
        iconName = require('../assets/content/female.png');
      }

      return <Image source={iconName} style={{width: 20, height: 20}} />
    },
  }),
  tabBarOptions: {
    style: {
      backgroundColor: '#61B046',
    },
    showIcon: true,
    showLabel: false
  }
}
);

export const ContentNavigator = StackNavigator({
  ContentMain: HomeTabs,
  ContentItem: { screen: SingleMain },
},
{
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});

export const GameNavigator = StackNavigator({
  GameOverview: { screen: GameOverview },
  StartLevel: { screen: StartLevel }
},
{
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});

export const CustomDrawer = DrawerNavigator({
  Home: { screen: InitialChoice },
  Content: ContentNavigator,
  Game: GameNavigator,
  About: { screen: AboutMain },
  GameInstructions: { screen: GameInstructions },
  ReadInstructions: { screen: ReadInstructions },
}, {
  contentComponent: SideMenu,
  drawerWidth: 300
});

export const CustomDrawerContent = DrawerNavigator({
  Content: ContentNavigator,
  Home: { screen: InitialChoice },
  Game: GameNavigator,
  About: { screen: AboutMain },
  GameInstructions: { screen: GameInstructions },
  ReadInstructions: { screen: ReadInstructions },
}, {
  contentComponent: SideMenu,
  drawerWidth: 300
});

export const CustomDrawerGame = DrawerNavigator({
  Game: GameNavigator,
  ReadInstructions: { screen: ReadInstructions },
  GameInstructions: { screen: GameInstructions },
  Content: ContentNavigator,
  Home: { screen: InitialChoice },
  About: { screen: AboutMain },
}, {
  contentComponent: SideMenu,
  drawerWidth: 300
});
