import { initialState } from './initialState';

const contentReducers = (state = initialState, action) => {
  let newState;
  switch (action.type) {
    case 'SET_CONTENT_DATA':
      newState = Object.assign({}, state, {data: action.data});
      return newState;
      break;
    case 'SET_CURRENT_NAV':
      newState = Object.assign({}, state, {nav: action.nav});
      return newState;
      break;
    default:
      return state
  }
}
export default contentReducers;
