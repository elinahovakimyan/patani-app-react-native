export const initialState = {
  data: [],
  question: {},
  levelOverview: {
    firstLevel: {
      points: 0,
      answers: [],
      hintOpened: false
    },
    secondLevel: {
      points: 0,
      answers: [],
      hintOpened: false
    },
    thirdLevel: {
      points: 0,
      answers: [],
      hintOpened: false
    },
    fourthLevel: {
      points: 0,
      answers: [],
      hintOpened: false
    }
  },
  nav: {
    state: {
      key: "Home",
      params: undefined,
      routeName: "Home",
    },
  }
}
