import { initialState } from './initialState';

const gameReducers = (state = initialState, action) => {
  let newState;
  switch (action.type) {
    case 'SET_CURRENT_QUESTION':
      newState = Object.assign({}, state, {question: action.question});
      return newState;
      break;
    case 'UPDATE_POINTS':
      newState = Object.assign({}, state, {levelOverview: action.levelOverview});
      return newState;
      break;
    default:
      return state
  }
}
export default gameReducers;
