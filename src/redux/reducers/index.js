import { combineReducers } from "redux";
import contentReducers from "./contentReducers";
import gameReducers from "./gameReducers";

const rootReducer = combineReducers({
  contentReducers,
  gameReducers
});

export default rootReducer;
