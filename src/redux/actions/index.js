export function setContentData(data) {
  return {
    type: 'SET_CONTENT_DATA',
    data
  }
}
