import * as Expo from "expo";
import React, { Component } from "react";
import * as Font from 'expo-font';
import App from '../App';

export default class Setup extends Component {
  constructor() {
    super();
    this.state = {
      isReady: false
    };
  }

  componentWillMount() {
    this.loadFonts();
  }

  async loadFonts() {
    await Font.loadAsync({
      Roboto: require("../assets/fonts/Roboto.ttf"),
      Roboto_medium: require("../assets/fonts/Roboto_medium.ttf"),
      DejavuSans: require("../assets/fonts/DejaVuSans.ttf"),
      DejavuSans_bold: require("../assets/fonts/DejaVuSans-Bold.ttf"),
      DejavuSans_bold_oblique: require("../assets/fonts/DejaVuSans-BoldOblique.ttf"),
      DejavuSans_extralight: require("../assets/fonts/DejaVuSans-ExtraLight.ttf"),
      DejavuSans_oblique: require("../assets/fonts/DejaVuSans-Oblique.ttf"),
      DejavuSans_condensed: require("../assets/fonts/DejaVuSansCondensed.ttf"),
      DejavuSans_condensed_bold: require("../assets/fonts/DejaVuSansCondensed-Bold.ttf"),
      DejavuSans_condensed_bold_oblique: require("../assets/fonts/DejaVuSansCondensed-BoldOblique.ttf"),
      DejavuSans_condensed_oblique: require("../assets/fonts/DejaVuSansCondensed-Oblique.ttf")
    });
    this.setState({ isReady: true });
  }
  render() {
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    }
    return (
      <App />
    );
  }
}
