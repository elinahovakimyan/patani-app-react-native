import React, { Component } from 'react';
import { Text, ScrollView } from 'react-native';
import { Card } from 'native-base';
import { connect } from 'react-redux';

class AboutMain extends Component {
  componentDidMount() {
    const { navigation } = this.props;
    this.props.setCurrentNav(navigation);
  }

  render () {
    return (
      <Card style={styles.wrapper}>
        <ScrollView style={{padding: 5}}>
          <Text style={styles.text}>
            Այս խաղը մշակվել է 11-13 տարեկան պատանիների համար, ովքեր կցանկանան մեզ հետ բացահայտել սեփական մարմնի ու հոգեվիճակի հետ կապված փոփոխությունները: Այն հնարավորություն կտա սովորել, մրցել, տեղեկանալ և փոխանցել գիտելիքներ, որոնք ձեռք կբերվեն խաղի ամեն փորձությունը հաղթահարելիս:
          </Text>
          <Text style={styles.text}>
            Խաղը մշակվել է Կանանց ռեսուրսային կենտրոնի և Girls in Tech Հայաստանի հետ համատեղ:
          </Text>
          <Text style={styles.text}>
            Խաղի մշակման ժամանակ ձևավորվել է պատանիներից և ծնողներից կազմված փորձագիտական խումբ, ովքեր իրենց աջակցությունն են ցուցաբերել խաղի մշակման գործընթացում:
          </Text>
        </ScrollView>
      </Card>
    )
  }
}

const styles = {
  wrapper: {
    marginRight: 10,
    marginLeft: 10,
    marginTop: 10,
    marginBottom: 12,
    padding: 12,
    borderRadius: 10,
    flex: 1
  },
  text: {
    fontFamily: 'DejavuSans',
    color: '#5e5f5e',
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setCurrentNav: (nav) => dispatch({ type: 'SET_CURRENT_NAV', nav }),
  }
}

export default connect(null, mapDispatchToProps)(AboutMain);
