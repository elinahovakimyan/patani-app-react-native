import React, { Component } from 'react';
import { ScrollView, View, Dimensions, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';

import { Image, ImageBackground } from 'react-native';
import { Container, DeckSwiper, Card, CardItem, Thumbnail, Text, Left, Body, Icon } from 'native-base';

const cards = [
  {
    id: 0,
    title: 'Ծանոթացիր խաղի կառուցվածքին և կանոններին',
    content: 'Խաղն ունի 4 փուլ, յուրաքանչյուր փուլում՝ 10 հարց: Հարցի ճիշտ պատասխանը քեզ կտա 3 միավոր, իսկ սխալը՝ 0: Կան հարցեր որոնք ունեն <<Հուշում>>՝ քեզ օգնելու գտնել ճիշտ պատասխանը: <<Հուշում>>-ից օգտվելու դեպքում դու ունես հանավորություն վաստակել առավելագույնը 1 միավոր: ',
    image: require('../../assets/ic_play_1_xhdpi.png'),
  },
  {
    id: 1,
    title: 'Ծանոթացիր խաղի կառուցվածքին և կանոններին',
    content: 'Յուրաքանչյուր հարցին պատասխան տալուց հետո կարող ես շարժվել միայն առաջ ՝ սեղմելով <<Հաջորդը>>:',
    image: require('../../assets/ic_play_2_xhdpi.png'),
  },
  {
    id: 2,
    text: 'Ծանոթացիր խաղի կառուցվածքին և կանոններին',
    whiteContent: 'Առաջին փուլը կհաղթահարես, եթե վաստակես նվազագույնը 27 միավոր հնարավոր 30-ից, երկրորդ փուլում՝ 28, երրորդում՝ 29, և վերջին փուլում՝ 30: Այն պահին երբ դադարեցնես խաղալը, վաստակածդ միավորները կպահպանվեն և կկարողանաս ցանկացած պահի շարունակել խաղը: Բոլոր փուլերը հաջողությամբ ավարտելուց հետո, կտեսնես խաղիդ ամբողջական միավորը:',
    msg: 'Դե ինչ, քեզ հաջողություն :)',
    button: 'սկսել խաղը',
    image: require('../../assets/ic_play_3_xhdpi.png'),
  }
];

class GameInstructions extends Component {
  componentDidMount() {
    const { navigation } = this.props;
    this.props.setCurrentNav(navigation);
  }

  render() {
    return (
        <DeckSwiper
          dataSource={cards}
          renderItem={item =>
            <View style={[{backgroundColor: 'white'}, styles.instImg]}>
              <Image style={styles.instImg} source={item.image} />
              {!!item.button &&
                <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate('GameOverview')}>
                </TouchableOpacity>
              }
            </View>
          }
        />
    );
  }
}

const width = Dimensions.get('window').width; //full width
const height = Dimensions.get('window').height; //full width

const styles = {
  wrapper: {
    // flex: 1,
    width: width,
    // height: height - 75,
    // flexDirection: 'row',
    // justifyContent: 'space-between',
    // alignItems: 'flex-end',
    // position: 'absolute',
    // bottom: 0
  },
  // bottomText: {
  //   flex: 1,
  //   flexDirection: 'row',
  //   justifyContent: 'space-between',
  //   alignItems: 'flex-end',
  //   position: 'absolute',
  //   bottom: 60,
  //   padding: 20,
  //   color: 'white',
  //   fontSize: 14,
  //   textAlign: 'center',
  //   fontFamily: 'DejavuSans'
  // },
  instImg: {
    width: width,
    height: height - 75,
    marginTop: 2,
    backgroundColor: 'white',
    position: 'relative'
    // flex: 1,
    // flexDirection: 'row',
    // justifyContent: 'space-between',
    // alignItems: 'flex-end',
    // position: 'absolute',
    // bottom: 0
  },
  button: {
    height: 100,
    width,
    position: 'absolute',
    bottom: 20,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  text: {
    fontFamily: 'DejavuSans',
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setCurrentNav: (nav) => dispatch({ type: 'SET_CURRENT_NAV', nav }),
  }
}

export default connect(null, mapDispatchToProps)(GameInstructions);
