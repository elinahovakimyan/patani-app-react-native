import React, { Component } from 'react';
import { ScrollView, View, Dimensions } from 'react-native';
import { connect } from 'react-redux';

import { Image, ImageBackground } from 'react-native';
import { Container, DeckSwiper, Card, CardItem, Thumbnail, Text, Left, Body, Icon } from 'native-base';

const cards = [
  {
    id: 0,
    title: 'խաղի կառուցվածքին և կանոններին',
    content: 'Խաղն ունի 4 փուլ, յուրաքանչյուր փուլում՝ 10 հարց: Հարցի ճիշտ պատասխանը քեզ կտա 3 միավոր, իսկ սխալը՝ 0: Կան հարցեր որոնք ունեն <<Հուշում>>՝ քեզ օգնելու գտնել ճիշտ պատասխանը: <<Հուշում>>-ից օգտվելու դեպքում դու ունես հանավորություն վաստակել առավելագույնը 1 միավոր: ',
    image: require('../../assets/ic_read_xhdpi.png'),
  }
];

class ReadInstructions extends Component {
  componentDidMount() {
    const { navigation } = this.props;
    this.props.setCurrentNav(navigation);
  }

  render() {
    return (
      <ScrollView style={styles.scrollWrap}>
        <Image style={styles.instImg} source={require('../../assets/ic_read_xhdpi.png')} />
      </ScrollView>
    );
  }
}

const width = Dimensions.get('window').width; //full width
const height = Dimensions.get('window').height; //full height

const styles = {
  scrollWrap: {
    backgroundColor: 'white',
    marginTop: 5
  },
  instImg: {
    width: width,
    height: height - 35,
    backgroundColor: 'white',
    marginTop: 2
    // flex: 1,
    // flexDirection: 'row',
    // justifyContent: 'space-between',
    // alignItems: 'flex-end',
    // position: 'absolute',
    // bottom: 0
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setCurrentNav: (nav) => dispatch({ type: 'SET_CURRENT_NAV', nav }),
  }
}

export default connect(null, mapDispatchToProps)(ReadInstructions);
