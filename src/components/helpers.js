export function areEqualArrays(arr1, arr2, depth) {
  if(depth === 1) {
    for(var i = arr1.length; i--;) {
      if(typeof arr1[i] !== "string" || typeof arr1[i] !== "string") {
        return false;
      } else if(typeof arr1[i] === "string" && typeof arr1[i] === "string" && arr1[i].toUpperCase() !== arr2[i].toUpperCase()) {
        return false;
      } else if(arr1[i] !== arr2[i]) {
        return false;
      }
      return true;
    }
  } else if(depth === 2) {
    for(var n = arr1.length; n--;) {
      for(var i = arr1[n].length; i--;) {
        if(typeof arr1[i] !== "string" || typeof arr1[i] !== "string") {
          return false;
        } else if(typeof arr1[i] === "string" && typeof arr1[i] === "string" && arr1[i].toUpperCase() !== arr2[i].toUpperCase()) {
          return false;
        } else if(arr1[i] !== arr2[i]) {
          return false;
        }
        return true;
      }
    }
  }
}
