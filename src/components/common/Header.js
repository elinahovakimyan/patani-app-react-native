import React from 'react';
import { Button, View, Image, TouchableOpacity, AsyncStorage, Text, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import Hint from '../game/levels/Hint';

class Header extends React.Component {

  handleLvlLeave = async () => {
    const { levelOverview } = this.props;
    this.props.nav.navigate('GameOverview');

    if(levelOverview.firstLevel.answers.length > 0) {
      try {
        await AsyncStorage.setItem('@MySuperStore:gameData', JSON.stringify(levelOverview));
      } catch (error) {
        console.log(error);
      }
    }
  }

  render() {
    const { navigate, goBack, state } = this.props.nav;
    const { question } = this.props;
    let levelTitle = '';

    if(!!state.params && !!state.params.level && state.params.level.includes("first")) {
      levelTitle = '1-ին փուլ'
    } else if(!!state.params && !!state.params.level && state.params.level.includes("second")) {
      levelTitle = '2-րդ փուլ'
    } else if(!!state.params && !!state.params.level && state.params.level.includes("third")) {
      levelTitle = '3-րդ փուլ'
    } else if(!!state.params && !!state.params.level && state.params.level.includes("fourth")) {
      levelTitle = '4-րդ փուլ'
    }

    if(state.routeName === 'ContentItem') {
      return(
        <View style={styles.wrapperBack}>
          <TouchableOpacity onPress={() => navigate(state.params.key || 'ContentItem')} style={styles.backWrap}>
            <Image source={require('../../assets/ic_back_xhdpi.png')} style={styles.imgBack}/>
          </TouchableOpacity>
          <Text style={styles.contentTitle}>{state.params.title}</Text>
        </View>
      )
    } else if(state.routeName.includes('Level')) {
      return(
        <View style={styles.wrapperBack}>
          <TouchableOpacity onPress={() => this.handleLvlLeave()} style={styles.backWrap}>
            <Image source={require('../../assets/ic_back_xhdpi.png')} style={styles.imgBack}/>
          </TouchableOpacity>
          {!!levelTitle &&
            <Text style={styles.contentTitle}>{levelTitle.toUpperCase()}</Text>
          }
          {!!levelTitle && !!question && !!question.hint &&
            <Hint hintContent={question.hint} level={state.params.level} />
          }
        </View>
      )
    } else if(state.routeName !== 'Home') {
      return(
        <View style={styles.wrapperBack}>
          <TouchableOpacity onPress={() => navigate('DrawerOpen')} style={styles.wrapper}>
            <Image source={require('../../assets/ic_menu_xhdpi.png')} style={styles.imgDrawer}/>
          </TouchableOpacity>

          {(state.routeName === 'GameOverview' || state.routeName === 'GameInstructions') &&
            <Text style={styles.contentTitle}>ԽԱՂԱԼ</Text>
          }

          {(state.routeName === 'Male' || state.routeName === 'Female' || state.routeName === 'ReadInstructions') &&
            <Text style={styles.contentTitle}>ԿԱՐԴԱԼ</Text>
          }

          {state.routeName === 'About' &&
            <Text style={styles.contentTitle}>ՄԵՐ ՄԱՍԻՆ</Text>
          }

          {(state.routeName.includes('Male') || state.routeName.includes('Female')) &&
            <TouchableOpacity onPress={() => navigate('ReadInstructions')} style={styles.hintStyle}>
              <Image source={require('../../assets/game/ic_hint_xhdpi.png')} style={styles.openBtn}/>
            </TouchableOpacity>
          }
          {state.routeName.includes('GameOverview') &&
            <TouchableOpacity onPress={() => navigate('GameInstructions')} style={styles.hintStyle}>
              <Image source={require('../../assets/game/ic_hint_xhdpi.png')} style={styles.openBtn}/>
            </TouchableOpacity>
          }
        </View>
      )
    } else {
      return(
        <View></View>
      )
    }

  }
}

const width = Dimensions.get('window').width; //full width

const styles = {
  wrapper: {
    backgroundColor: '#61B046',
    height: 50,
    padding: 9,
    paddingStart: 15
  },
  wrapperBack: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'space-between',
    height: 50,
    padding: 2,
    marginBottom: 0,
    backgroundColor: '#61B046',
  },
  hintStyle: {
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    marginLeft: 'auto',
    marginRight: 12
  },
  openBtn: {
    width: 27,
    height: 27
  },
  backWrap: {
    height: 40,
    width: 40,
    paddingTop: 10,
    paddingStart: 15
  },
  imgBack: {
    height: 20,
    width: 20,
  },
  contentTitle: {
    paddingStart: 8,
    fontSize: 18,
    color: '#fff',
    textAlign: 'left',
    fontFamily: 'DejavuSans_bold'
  },
  imgDrawer: {
    marginTop: 5,
    paddingStart: 15,
    height: 25,
    width: 25
  },
  navItemWrap: {
    paddingStart: 20,
    padding: 10
  },
  navItem: {
    flex: 1,
    paddingStart: 30,
    bottom: 40
  }
}

function mapStateToProps(state) {
  return {
    nav: state.contentReducers.nav,
    question: state.gameReducers.question,
    levelOverview: state.gameReducers.levelOverview
  }
}

// withNavigation returns a component that wraps MyBackButton and passes in the
// navigation prop
export default connect(mapStateToProps)(Header);
