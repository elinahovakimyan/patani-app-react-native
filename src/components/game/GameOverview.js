import React from 'react';
import { Text, View, ScrollView, TouchableOpacity, AsyncStorage, ImageBackground, Image } from 'react-native';
import { connect } from 'react-redux';
import data from '../../data/game.json';

class GameOverview extends React.Component {
  componentDidMount() {
    const { navigation } = this.props;
    this.props.setCurrentNav(navigation);
  }

  render() {
    const { navigation, levelOverview } = this.props;

    return(
      <ScrollView style={{backgroundColor: 'white'}}>
        <View style={[styles.gameWrap, {marginTop: 30}]}>
          <TouchableOpacity onPress={() => navigation.navigate('StartLevel', {level: "firstLevel"})}>
            <ImageBackground source={require('../../assets/game/overview/ic_level_1_xhdpi.png')} style={styles.gameImg}>
              {levelOverview.firstLevel.points < 10 &&
                <Image source={require('../../assets/game/overview/ic_0_stars_xhdpi.png')} style={styles.countImg}/>
              }
              {levelOverview.firstLevel.points >= 10 && levelOverview.firstLevel.points < 20 &&
                <Image source={require('../../assets/game/overview/ic_1_stars_xhdpi.png')} style={styles.countImg}/>
              }
              {levelOverview.firstLevel.points >= 20 && levelOverview.firstLevel.points < 25 &&
                <Image source={require('../../assets/game/overview/ic_2_stars_xhdpi.png')} style={styles.countImg}/>
              }
              {levelOverview.firstLevel.points >= 25 &&
                <Image source={require('../../assets/game/overview/ic_3_stars_xhdpi.png')} style={styles.countImg}/>
              }
              <Text style={styles.countText}>{levelOverview.firstLevel.answers.length}/10</Text>
            </ImageBackground>
          </TouchableOpacity>
          {levelOverview.firstLevel.points < 25 &&
            <Image source={require('../../assets/game/overview/ic_locked_level_2_xhdpi.png')} style={styles.gameImg}/>
          }
          {levelOverview.firstLevel.points >= 25 &&
            <TouchableOpacity onPress={() => navigation.navigate('StartLevel', {level: "secondLevel"})}>
              <ImageBackground source={require('../../assets/game/overview/ic_level_2_xhdpi.png')} style={styles.gameImg}>
                {levelOverview.secondLevel.points < 10 &&
                  <Image source={require('../../assets/game/overview/ic_0_stars_xhdpi.png')} style={styles.countImg}/>
                }
                {levelOverview.secondLevel.points >= 10 && levelOverview.secondLevel.points < 20 &&
                  <Image source={require('../../assets/game/overview/ic_1_stars_xhdpi.png')} style={styles.countImg}/>
                }
                {levelOverview.secondLevel.points >= 20 && levelOverview.secondLevel.points < 27 &&
                  <Image source={require('../../assets/game/overview/ic_2_stars_xhdpi.png')} style={styles.countImg}/>
                }
                {levelOverview.secondLevel.points >= 25 &&
                  <Image source={require('../../assets/game/overview/ic_3_stars_xhdpi.png')} style={styles.countImg}/>
                }
                <Text style={styles.countText}>{levelOverview.secondLevel.answers.length}/10</Text>
              </ImageBackground>
            </TouchableOpacity>
          }
        </View>
        <View style={styles.gameWrap}>
          {levelOverview.secondLevel.points < 25 &&
            <Image source={require('../../assets/game/overview/ic_locked_level_3_xhdpi.png')} style={styles.gameImg}/>
          }
          {levelOverview.secondLevel.points >= 25 &&
            <TouchableOpacity onPress={() => navigation.navigate('StartLevel', {level: "thirdLevel"})}>
              <ImageBackground source={require('../../assets/game/overview/ic_level_3_xhdpi.png')} style={styles.gameImg}>
                {levelOverview.thirdLevel.points < 10 &&
                  <Image source={require('../../assets/game/overview/ic_0_stars_xhdpi.png')} style={styles.countImg}/>
                }
                {levelOverview.thirdLevel.points >= 10 && levelOverview.thirdLevel.points < 20 &&
                  <Image source={require('../../assets/game/overview/ic_1_stars_xhdpi.png')} style={styles.countImg}/>
                }
                {levelOverview.thirdLevel.points >= 20 && levelOverview.thirdLevel.points < 29 &&
                  <Image source={require('../../assets/game/overview/ic_2_stars_xhdpi.png')} style={styles.countImg}/>
                }
                {levelOverview.thirdLevel.points >= 25 &&
                  <Image source={require('../../assets/game/overview/ic_3_stars_xhdpi.png')} style={styles.countImg}/>
                }
                <Text style={styles.countText}>{levelOverview.thirdLevel.answers.length}/10</Text>
              </ImageBackground>
            </TouchableOpacity>
          }
          {levelOverview.thirdLevel.points < 25 &&
            <Image source={require('../../assets/game/overview/ic_locked_level_4_xhdpi.png')} style={styles.gameImg}/>
          }
          {levelOverview.thirdLevel.points >= 25 &&
            <TouchableOpacity onPress={() => navigation.navigate('StartLevel', {level: "fourthLevel"})}>
              <ImageBackground source={require('../../assets/game/overview/ic_level_4_xhdpi.png')} style={styles.gameImg}>
                {levelOverview.fourthLevel.points < 10 &&
                  <Image source={require('../../assets/game/overview/ic_0_stars_xhdpi.png')} style={styles.countImg}/>
                }
                {levelOverview.fourthLevel.points >= 10 && levelOverview.fourthLevel.points < 20 &&
                  <Image source={require('../../assets/game/overview/ic_1_stars_xhdpi.png')} style={styles.countImg}/>
                }
                {levelOverview.fourthLevel.points >= 20 && levelOverview.fourthLevel.points < 30 &&
                  <Image source={require('../../assets/game/overview/ic_2_stars_xhdpi.png')} style={styles.countImg}/>
                }
                {levelOverview.fourthLevel.points >= 25 &&
                  <Image source={require('../../assets/game/overview/ic_3_stars_xhdpi.png')} style={styles.countImg}/>
                }
                <Text style={styles.countText}>{levelOverview.fourthLevel.answers.length}/10</Text>
              </ImageBackground>
            </TouchableOpacity>
          }
        </View>
      </ScrollView>
    )
  }
}

const styles = {
  gameWrap: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginRight: 10,
    marginLeft: 10,
    marginBottom: 8
  },
  gameImg: {
    width: 180,
    height: 220
  },
  countText: {
    textAlign: 'center',
    top: 165,
    color: '#5e5f5e',
    // fontFamily: 'DejavuSans_bold'
  },
  countImg: {
    width: 90,
    height: 22,
    top: 160,
    left: 45
  }
}

function mapStateToProps(state) {
  return {
    levelOverview: state.gameReducers.levelOverview
  }
}

function mapDispatchToProps(dispatch) {
  return {
    updatePoints: (levelOverview) => dispatch({ type: 'UPDATE_POINTS', levelOverview }),
    setCurrentNav: (nav) => dispatch({ type: 'SET_CURRENT_NAV', nav }),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GameOverview);
