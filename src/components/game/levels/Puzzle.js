import React from 'react';
import {
  Text, View, ScrollView, TextInput, Image, TouchableOpacity, AsyncStorage, Dimensions
} from 'react-native';
import SortableGrid from 'react-native-sortable-grid'

class Puzzle extends React.Component {
  state = {
    puzzlePieces: [],
    correctOrder: [2,1,3,4,5,6,7,8,0],
    fullPic: null,
    puzzleAnswer: false
  }

  componentDidMount() {
    if(this.props.currentData.id === 15) {
      puzzlePieces = [
        require('../../../assets/game/puzzle15/ic_part_15_xhdpi.png'),
        require('../../../assets/game/puzzle15/ic_part_5_xhdpi.png'),
        require('../../../assets/game/puzzle15/ic_part_11_xhdpi.png'),
        require('../../../assets/game/puzzle15/ic_part_2_xhdpi.png'),
        require('../../../assets/game/puzzle15/ic_part_16_xhdpi.png'),
        require('../../../assets/game/puzzle15/ic_part_12_xhdpi.png'),
        require('../../../assets/game/puzzle15/ic_part_1_xhdpi.png'),
        require('../../../assets/game/puzzle15/ic_part_4_xhdpi.png'),
        require('../../../assets/game/puzzle15/ic_part_7_xhdpi.png'),
        require('../../../assets/game/puzzle15/ic_part_13_xhdpi.png'),
        require('../../../assets/game/puzzle15/ic_part_9_xhdpi.png'),
        require('../../../assets/game/puzzle15/ic_part_3_xhdpi.png'),
        require('../../../assets/game/puzzle15/ic_part_14_xhdpi.png'),
        require('../../../assets/game/puzzle15/ic_part_6_xhdpi.png'),
        require('../../../assets/game/puzzle15/ic_part_10_xhdpi.png'),
        require('../../../assets/game/puzzle15/ic_part_8_xhdpi.png'),
      ];
      fullPic = require('../../../assets/game/question15.png')
      this.setState({puzzlePieces, fullPic})
    } else if(this.props.currentData.id === 25) {
      puzzlePieces = [
        require('../../../assets/game/puzzle25/ic_3_q25_xhdpi.png'),
        require('../../../assets/game/puzzle25/ic_6_q25_xhdpi.png'),
        require('../../../assets/game/puzzle25/ic_8_q25_xhdpi.png'),
        require('../../../assets/game/puzzle25/ic_12_q25_xhdpi.png'),
        require('../../../assets/game/puzzle25/ic_15_q25_xhdpi.png'),
        require('../../../assets/game/puzzle25/ic_10_q25_xhdpi.png'),
        require('../../../assets/game/puzzle25/ic_2_q25_xhdpi.png'),
        require('../../../assets/game/puzzle25/ic_7_q25_xhdpi.png'),
        require('../../../assets/game/puzzle25/ic_13_q25_xhdpi.png'),
        require('../../../assets/game/puzzle25/ic_1_q25_xhdpi.png'),
        require('../../../assets/game/puzzle25/ic_5_q25_xhdpi.png'),
        require('../../../assets/game/puzzle25/ic_9_q25_xhdpi.png'),
        require('../../../assets/game/puzzle25/ic_16_q25_xhdpi.png'),
        require('../../../assets/game/puzzle25/ic_11_q25_xhdpi.png'),
        require('../../../assets/game/puzzle25/ic_4_q25_xhdpi.png'),
        require('../../../assets/game/puzzle25/ic_14_q25_xhdpi.png'),
      ];
      fullPic = require('../../../assets/game/question25.png')
      this.setState({puzzlePieces, fullPic})
    } else if(this.props.currentData.id === 30) {
      puzzlePieces = [
        require('../../../assets/game/puzzle30/ic_9_q30_xhdpi.png'),
        require('../../../assets/game/puzzle30/ic_5_q30_xhdpi.png'),
        require('../../../assets/game/puzzle30/ic_7_q30_xhdpi.png'),
        require('../../../assets/game/puzzle30/ic_12_q30_xhdpi.png'),
        require('../../../assets/game/puzzle30/ic_16_q30_xhdpi.png'),
        require('../../../assets/game/puzzle30/ic_3_q30_xhdpi.png'),
        require('../../../assets/game/puzzle30/ic_8_q30_xhdpi.png'),
        require('../../../assets/game/puzzle30/ic_1_q30_xhdpi.png'),
        require('../../../assets/game/puzzle30/ic_11_q30_xhdpi.png'),
        require('../../../assets/game/puzzle30/ic_2_q30_xhdpi.png'),
        require('../../../assets/game/puzzle30/ic_15_q30_xhdpi.png'),
        require('../../../assets/game/puzzle30/ic_4_q30_xhdpi.png'),
        require('../../../assets/game/puzzle30/ic_10_q30_xhdpi.png'),
        require('../../../assets/game/puzzle30/ic_6_q30_xhdpi.png'),
        require('../../../assets/game/puzzle30/ic_13_q30_xhdpi.png'),
        require('../../../assets/game/puzzle30/ic_14_q30_xhdpi.png'),
      ];
      fullPic = require('../../../assets/game/question30.png')
      this.setState({puzzlePieces, fullPic})
    }
  }

  areEqualArrays = (arr1, arr2) => {
    let areEqual = [];
    for(var i = arr1.length; i--;) {
      if(Number(arr1[i]) !== Number(arr2[i])) {
        areEqual.push(false);
      } else if(arr1[i].toString() !== arr2[i].toString()) {
        areEqual.push(false);
      }
      areEqual.push(true);
    }

    if(areEqual.includes(false)) {
      return false;
    } else {
      return true;
    }
  }

  handleDrag = (order) => {
    const currentOrder = order.itemOrder.map(item => item.key);
    const puzzleAnswer = this.areEqualArrays(this.props.currentData.correctOrder, currentOrder, 1);
    this.setState({puzzleAnswer});
    this.props.onPuzzleChange(puzzleAnswer);
  }


  render() {
    const { puzzleAnswer, fullPic, puzzlePieces } = this.state;
    const { id } = this.props.currentData;
    let puzzleWidth = 308, puzzleHeight = 308;

    if(id === 25) {
      puzzleWidth = 285;
    } else if(id === 30) {
      puzzleWidth = 320;
      puzzleHeight = 215;
    }

    return (
      <ScrollView>
        {puzzleAnswer === true &&
          <Image source={fullPic} style={{width: puzzleWidth, height: puzzleHeight}}/>
        }
        {puzzleAnswer !== true &&
          <SortableGrid
            onDragRelease={this.handleDrag}
            itemsPerRow={4}
          >
            {puzzlePieces.map((url, index) =>
              <View key={index} value={url}>
                <Image
                  source={url}
                  style={{width: 76, height: 76}}
                />
              </View>
            )}
          </SortableGrid>
        }
      </ScrollView>
    )
  }
}


export default Puzzle;
