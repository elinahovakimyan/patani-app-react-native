import React from 'react';
import { View, Text, TouchableOpacity, Image, Dimensions } from 'react-native';
import SortableGrid from 'react-native-sortable-grid'

class Taraxax extends React.Component {
  state = {
    currentWord: '',
    completedWords: []
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { currentData } = this.props;
    const { currentWord, completedWords } = this.state;

    if(prevState.currentWord !== currentWord) {
      if(currentData.answers.includes(currentWord.toLowerCase()) && !completedWords.includes(currentWord)) {
        this.setState({completedWords: [currentWord, ...completedWords], currentWord: ''});
        this.props.handleTaraxaxChange(completedWords.includes(currentWord) ? completedWords : [currentWord, ...completedWords])
      }
    }
  }

  handleTouch = (e, val) => {
    const currentWord = this.state.currentWord + val;
    this.setState({currentWord});
  }

  handleBackspace = () => {
    let slicedStr = this.state.currentWord.slice(0, -1);
    this.setState({currentWord: slicedStr});
  }


  render() {
    const { currentData } = this.props;
    const { currentWord, completedWords } = this.state;

    return(
      <View>
        {!!currentData.questionList && currentData.questionList.map((item, i) => (
          <Text style={styles.questions} key={item}>{item}</Text>
        ))}

        {currentData.board.map((row, i) => {
          return(
            <View style={styles.taraxaxWrap} key={i}>
              {!!row && row.map((field, index) => {
                return(
                  <TouchableOpacity style={styles.taraxaxSquareWrap} key={index+field+i} onPress={(e) => this.handleTouch(e, field)}>
                    <Text style={styles.taraxaxSquare}>{field}</Text>
                  </TouchableOpacity>
                )
              })}
            </View>
          )
        })}

        <View style={styles.typingWordWrap}>

        {!!currentWord &&
            <Text style={styles.typingWord}>{currentWord} </Text>
          }
            {!!currentWord &&
            <TouchableOpacity style={styles.backIcWrap} onPress={this.handleBackspace}>
              <Image style={styles.backIc}  source={require('../../../assets/game/ic_back_delete_xhdpi.png')}/>
            </TouchableOpacity>
        }

        {!currentWord &&
          <Text style={styles.typingWord}>|</Text>
        }

      </View>

        {completedWords.map(word => <Text key={word} style={styles.correctWords}>{word}</Text>)}
      </View>
    )
  }
}

const width = Dimensions.get('window').width; //full width

const styles = {
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems:'center'
  },
  typingWordWrap: {
    flexDirection: 'row',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderColor: '#61B046',
    margin: 10
  },
  taraxaxWrap: {
    flex: 1,
    flexDirection: 'row',
    alignItems:'center',
    justifyContent: 'center',
    marginTop: 5
  },
  questions: {
    fontFamily: 'DejavuSans_oblique',
    color: '#5e5f5e'
  },
  backIc: {
    width: 20,
    height: 10,
  },
  backIcWrap: {
    alignSelf: 'flex-end',
    marginBottom: 7,
    marginLeft: 10
  },
  typingWord: {
    textAlign: 'center',
    color: '#5e5f5e',
    fontFamily: 'DejavuSans_bold',
    marginTop: 10
  },
  correctWords: {
    fontFamily: 'DejavuSans_bold',
    backgroundColor: 'rgba(97, 176, 70, 0.5)',
    color: '#5e5f5e',
    borderRadius: 10,
    padding: 5,
    margin: 10,
    textAlign: 'center'
  },
  taraxaxSquare: {
    // width: 20,
    // height: 20,
    // borderColor: '#61B064',
    // borderWidth: 0.5,
    color: '#5e5f5e',
    fontFamily: 'DejavuSans_bold',
    textAlign: 'center'
  },
  taraxaxSquareWrap: {
    width: (width - 40)/10,
    height: 20,
  }
}


export default Taraxax;
