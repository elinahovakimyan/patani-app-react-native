import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, Dimensions } from 'react-native';
import Modal from "react-native-modal";
import { connect } from 'react-redux';

class Hint extends Component {
  state = {
    isModalVisible: false
  };

  openModal = () => {
    const { level, levelOverview } = this.props;
    this.setState({ isModalVisible: true });
    levelOverview[level].hintOpened = true;
  }

  toggleModal = () => this.setState({ isModalVisible: !this.state.isModalVisible });

  render() {
    const { hintContent, levelOverview } = this.props;
    return [
      <TouchableOpacity key="trigger" onPress={this.openModal} style={styles.hintStyle}>
        <Image style={styles.openBtn} source={require('../../../assets/game/ic_hint_xhdpi.png')}/>
      </TouchableOpacity>,
      <Modal key="modal" isVisible={this.state.isModalVisible}
             style={styles.modalWrapper}
             onBackdropPress={() => this.setState({ isVisible: false })}>
        <View style={styles.modalContent}>
          <TouchableOpacity onPress={this.toggleModal}>
            <Image source={require('../../../assets/menu/ic_close_xhdpi.png')} style={styles.modalClose}/>
          </TouchableOpacity>
          <Text style={styles.modalTitle}>Հուշում</Text>
          <Text style={styles.modalContentText}>{hintContent}</Text>
        </View>
      </Modal>
    ]
  }
}

const width = Dimensions.get('window').width; //full width

const styles = {
  modalClose: {
    width: 13,
    height: 13,
    alignSelf: 'flex-end',
  },
  openBtn: {
    width: 27,
    height: 27
  },
  hintStyle: {
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    marginLeft: 'auto',
    marginRight: 12
  },
  modalContent: {
    backgroundColor: '#fff',
    padding: 8,
    borderRadius: 10
  },
  modalTitle: {
    fontFamily: 'DejavuSans_bold',
    textAlign: 'center',
    color: '#61B046',
    fontSize: 18,
    alignSelf: 'center',
    borderBottomWidth: 1,
    borderColor: '#B0D8A3',
    width: width - 50,
    marginBottom: 5,
    paddingBottom: 5
  },
  modalContentText: {
    textAlign: 'center',
    fontFamily: 'DejavuSans',
    color: '#5e5f5e',
    padding: 10
  },
  boldText: {
    fontFamily: 'DejavuSans_bold'
  }
}


function mapStateToProps(state) {
  return {
    levelOverview: state.gameReducers.levelOverview
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setCurrentNav: (nav) => dispatch({ type: 'SET_CURRENT_NAV', nav }),
    setCurrentQuestion: (question) => dispatch({ type: 'SET_CURRENT_QUESTION', question }),
    updatePoints: (levelOverview) => dispatch({ type: 'UPDATE_POINTS', levelOverview }),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Hint);
