import React from 'react';
import { Text, View, ScrollView, TouchableOpacity } from 'react-native';

class FormWord extends React.Component {
  state = {
    colorTheme: {}
  }

  handleFormWordChange = (letter, type, i) => {
    const { colorTheme } = this.state;
    const { formWordBase, formWordSelected, currentData } = this.props;

    if(Object.keys(colorTheme).length > 0) {
      this.setState({colorTheme: {}})
    }

    if(type === 'add' && formWordBase.length > 1) {
      let filteredWordBase = [];
      filteredWordBase = formWordBase.filter((item, ind) => ind !== i);
      this.props.handleChange({
        formWordSelected: [...formWordSelected, letter],
        formWordBase: filteredWordBase,
      });
    } else if(type === 'add' && formWordBase.length === 1) {
      let filteredWordBase = [];
      filteredWordBase = formWordBase.filter((item, ind) => ind !== i);
      this.props.handleChange({
        formWordSelected: [...formWordSelected, letter],
        formWordBase: filteredWordBase,
      });

      var myWord;

      if(currentData.type === 'form-word') {
        myWord = (formWordBase.length > 0) ? (formWordSelected.join('') + letter) : formWordSelected.join('');
      }

      if(currentData.type === 'form-sentence') {
        myWord = (formWordBase.length > 0) ? (formWordSelected.join(' ') + ' ' + letter) : formWordSelected.join(' ');
      }

      if(myWord === currentData.answer) {
        this.setState({colorTheme: {text: {color: 'white'}, view: {backgroundColor: '#61B046'}}})
      } else {
        this.setState({colorTheme: {text: {color: 'white'}, view: {backgroundColor: 'red', borderColor: 'red'}}})
      }
    } else if(type === 'remove') {
      let newFormWordBase = formWordSelected.filter((item, ind) => ind !== i)
      this.props.handleChange({
        formWordSelected: newFormWordBase,
        formWordBase: [...formWordBase, letter]
      });
    }
  }

  render() {
    const { formWordBase, formWordSelected, currentData } = this.props;
    const { colorTheme } = this.state;

    return(
      <ScrollView>
        <View style={[styles.formWordWrap, {height: 100}]}>
          {formWordBase.map((letter, i) => {
            return(
              <TouchableOpacity key={i} onPress={() => this.handleFormWordChange(letter, 'add', i)} style={styles.formWordBox}>
                <Text style={{color: '#61B046'}}>{letter}</Text>
              </TouchableOpacity>
            )
          })}
        </View>
        <View style={[styles.formWordWrap, {height: 150}]}>
          <View style={[styles.formWordWrap, styles.innerWrap]}>
            {formWordSelected.map((letter, i) => {
              if(i < currentData.partsInArray.length) {
                return(
                  <TouchableOpacity
                    key={i}
                    onPress={() => this.handleFormWordChange(letter, 'remove', i)}
                    style={[styles.formWordBox, colorTheme.view]}
                  >
                    <Text style={[{color: '#61B046'}, colorTheme.text]}>{letter}</Text>
                  </TouchableOpacity>
                )
              }
            })}
          </View>
        </View>
      </ScrollView>
    )
  }
}

const styles = {
  formWordWrap: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  innerWrap: {
    paddingRight: 5,
    paddingLeft: 5,
    paddingBottom: 3,
    borderBottomWidth: 0.5,
    minWidth: 200,
    minHeight: 28,
    borderColor: '#61B046'
  },
  formWordBox: {
    flexDirection: 'column',
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 3,
    paddingBottom: 3,
    margin: 1,
    borderRadius: 2,
    borderColor: '#61B046',
    borderWidth: 1
  },
}

export default FormWord;
