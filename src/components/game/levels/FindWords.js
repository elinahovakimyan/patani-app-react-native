import React from 'react';
import { Text, View } from 'react-native';
import { Button } from 'native-base';

class FindWords extends React.Component {
  state = {
    optionStyle: {
      btn: {backgroundColor: '#fff'},
      text: {color: '#61B046'},
    }
  }

  toggleSelect = () => {
    const { option } = this.props;
    const { optionStyle } = this.state;

    if(optionStyle.btn.backgroundColor === '#61B046') {
      this.setState({
        optionStyle: {
          btn: {backgroundColor: '#fff'},
          text: {color: '#61B046'},
        }
      });
      this.props.onFindWordsChange(option, 'remove')
    } else {
      this.setState({
        optionStyle: {
          btn: {backgroundColor: '#61B046'},
          text: {color: '#fff'},
        }
      });
      this.props.onFindWordsChange(option, 'add')
    }

  }

  render() {
    const { option } = this.props;
    const { optionStyle } = this.state;

    return(
      <View style={{marginLeft: 30, marginRight: 30, marginBottom: 2}}>
        <Button style={[optionStyle.btn, styles.btn]}
                key={option.label}
                onPress={this.toggleSelect}
                transparent
                block>
          <Text style={[optionStyle.text, styles.text]}>{option.label}</Text>
        </Button>
      </View>
    )
  }
}

const styles = {
  btn: {
    borderWidth: 1,
    borderColor: '#61B046',
    borderRadius: 5,
    marginBottom: 10
  },
  text: {
    fontFamily: 'DejavuSans_bold'
  }
}

export default FindWords;
