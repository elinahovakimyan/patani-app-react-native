import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, Dimensions, ImageBackground } from 'react-native';
import { Button } from 'native-base';
import { connect } from 'react-redux';

class FinishLevel extends Component {
  componentDidMount() {
    const { points, levelOverview, level, updatePoints, navigation } = this.props;
    let minPoints = 25;

    if((!level.includes('four') && points > minPoints) || (level.includes('four') && points !== 30)) {
      return navigation.navigate('GameOverview');
    }
  }

  reset = () => {
    const { points, levelOverview, level, updatePoints, navigation } = this.props;
    levelOverview[level] = {
      points: 0,
      answers: [],
      hintOpened: false
    }
    updatePoints(levelOverview);
    navigation.navigate('StartLevel', {level})
  }

  render() {
    const { points, level, levelOverview } = this.props;
    const allPoints = levelOverview.firstLevel.points + levelOverview.secondLevel.points + levelOverview.thirdLevel.points + levelOverview.fourthLevel.points;
    let minPoints = 25;

    if(!level.includes('four') && points < minPoints) {
      return (
        <ImageBackground source={require('../../../assets/game/ic_not_enough_points_xhdpi.png')} style={styles.notEnough}>
          <Text style={styles.textStyle}>ՑԱՎՈՔ ՄԻԱՎՈՐՆԵՐԴ ԲԱՎԱՐԱՐ ՉԵՆ ՀԱՋՈՐԴ ՓՈՒԼ ԱՆՑՆԵԼՈՒ ՀԱՄԱՐ՝</Text>
          <Text style={styles.textStyle}>{points}/30 ՄԻԱՎՈՐ</Text>
          <ImageBackground source={require('../../../assets/game/ic_try_again_cloud_xhdpi.png')} style={styles.tryAgain}>
            <TouchableOpacity onPress={() => this.reset()} style={{marginTop: 28}}>
              <Text style={styles.tryAgainTxt}>ԿՐԿԻՆ ՓՈՐՁԵԼ</Text>
            </TouchableOpacity>
          </ImageBackground>
        </ImageBackground>
      );
    } else if(level.includes('four') && points > 25) {
      return(
        <ImageBackground source={require('../../../assets/game/ic_congrats_xhdpi.png')} style={styles.congrats}>
          <Text style={styles.pointsText}>{allPoints}/120</Text>
        </ImageBackground>
      )
    // } else if(level.includes('four') && points === 30) {
    //   return(
    //     <ImageBackground source={require('../../../assets/game/ic_congrats_xhdpi.png')} style={styles.congrats}>
    //     </ImageBackground>
    //   )
    } else {
      return <View></View>
    }
  }
}

const width = Dimensions.get('window').width; //full width
const height = Dimensions.get('window').height; //full height

const styles = {
  notEnough: {
    width: width,
    height: height - 50,
    marginTop: 0,
    paddingTop: 0,
    top: 0,
  },
  congrats: {
    width: width,
    height: height - 70,
    marginTop: 0,
    paddingTop: 0
  },
  tryAgain: {
    width: 220,
    height: 56,
    marginTop: 40,
    marginLeft: width/2 - 100
  },
  tryAgainTxt: {
    textAlign: 'center',
    fontFamily: 'DejavuSans_bold',
    textDecorationLine: 'underline',
    color: '#61B046',
    fontSize: 16
  },
  pointsText: {
    textAlign: 'center',
    fontFamily: 'DejavuSans_bold',
    color: '#61B046',
    marginTop: height - 180,
    fontSize: 20
  },
  textStyle: {
    fontFamily: 'DejavuSans_bold',
    color: '#5f5e5f',
    textAlign: 'center',
    marginTop: 30,
    paddingStart: 20,
    paddingEnd: 20,
  }
}


function mapStateToProps(state) {
  return {
    levelOverview: state.gameReducers.levelOverview
  }
}

function mapDispatchToProps(dispatch) {
  return {
    updatePoints: (levelOverview) => dispatch({ type: 'UPDATE_POINTS', levelOverview }),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FinishLevel);
