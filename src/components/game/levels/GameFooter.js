import React from 'react';
import { Text, View, Dimensions, Image, StyleSheet } from 'react-native';
import { Button } from 'native-base';

class GameFooter extends React.Component {

  render() {
    const { index, level, levelOverview, onSubmit, onBack } = this.props;
    const points = levelOverview[level].points;
    return(
      <View style={styles.btnFooter}>
        {index <= 9 && index > 0 &&
          <Button onPress={() => onBack()} style={styles.btnStyle2} transparent success>
            <Text style={styles.btnText}>
              <Image style={styles.nextIcon} source={require('../../../assets/game/ic_prev_button_xhdpi.png')}/>
              {' '} Նախորդը
            </Text>
          </Button>
        }
        <Text style={[styles.btnStyle1, styles.btnText]}>
          {points}/30
        </Text>
        <Button onPress={() => onSubmit()} style={styles.btnStyle2} transparent success>
          {index < 9 &&
            <Text style={styles.btnText}>
              Հաջորդը {' '}
              <Image style={styles.nextIcon} source={require('../../../assets/game/ic_next_button_xhdpi.png')}/>
            </Text>
          }
          {index === 9 &&
            <Text style={styles.btnText}>
              Ավարտել
            </Text>
          }
        </Button>
      </View>
    )
  }
}

const width = Dimensions.get('window').width; //full width

const styles = StyleSheet.create({
  btnFooter: {
    flex: 1,
    width: width - 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    position: 'absolute',
    bottom: 0
  },
  btnStyle1: {
    alignSelf: 'flex-start',
  },
  btnStyle2: {
    alignSelf: 'flex-end',
  },
  btnText: {
    color: '#61B046',
    padding: 12,
  },
  nextIcon: {
    width: 27,
    height: 25
  }
})

export default GameFooter;
