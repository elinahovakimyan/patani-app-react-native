import React from 'react';
import { View, TextInput, Text } from 'react-native';

class GuessWord extends React.Component {

  render() {
    const { currentData, handleGuessChange } = this.props;
    let inputNum = 0;
    return(
      <View style={{flexDirection: 'row', justifyContent: 'center', marginTop: 100}}>
        {currentData.answer.toLowerCase() === 'դեռահասություն' && currentData.structure.map((item, i) => {
          if(!!item) {
            return <Text key={i} style={[styles.guessSquare, styles.guessSquareLetter]}>{item}</Text>
          } else {
            inputNum++;

            if(inputNum === 0) {
              return(
                <TextInput key={i} style={styles.guessSquare}
                  maxLength={1}
                  ref={(input) => { this.firstTextInput = input; }}
                  blurOnSubmit={false}
                  returnKeyType = { "next" }
                  onSubmitEditing={() => { this.secondTextInput.focus(); }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(letter) => handleGuessChange(letter, i)} />
              )
            } else if(inputNum === 1) {
              return(
                <TextInput key={i} style={styles.guessSquare}
                  maxLength={1}
                  ref={(input) => { this.secondTextInput = input; }}
                  blurOnSubmit={false}
                  returnKeyType = { "next" }
                  onSubmitEditing={() => { this.thirdTextInput.focus(); }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(letter) => handleGuessChange(letter, i)} />
              )
            } else if(inputNum === 2) {
              return(
                <TextInput key={i} style={styles.guessSquare}
                  maxLength={1}
                  ref={(input) => { this.thirdTextInput = input; }}
                  blurOnSubmit={false}
                  returnKeyType = { "next" }
                  onSubmitEditing={() => { this.fourthTextInput.focus(); }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(letter) => handleGuessChange(letter, i)} />
              )
            } else if(inputNum === 3) {
              return(
                <TextInput key={i} style={styles.guessSquare}
                  maxLength={1}
                  ref={(input) => { this.fourthTextInput = input; }}
                  blurOnSubmit={false}
                  returnKeyType = { "next" }
                  onSubmitEditing={() => { this.fifthTextInput.focus(); }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(letter) => handleGuessChange(letter, i)} />
              )
            } else if(inputNum === 4) {
              return(
                <TextInput key={i} style={styles.guessSquare}
                  maxLength={1}
                  ref={(input) => { this.fifthTextInput = input; }}
                  blurOnSubmit={false}
                  returnKeyType = { "next" }
                  onSubmitEditing={() => { this.sixthTextInput.focus(); }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(letter) => handleGuessChange(letter, i)} />
              )
            } else if(inputNum === 5) {
              return(
                <TextInput key={i} style={styles.guessSquare}
                  maxLength={1}
                  ref={(input) => { this.sixthTextInput = input; }}
                  blurOnSubmit={false}
                  returnKeyType = { "next" }
                  onSubmitEditing={() => { this.seventhTextInput.focus(); }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(letter) => handleGuessChange(letter, i)} />
              )
            } else if(inputNum === 6) {
              return(
                <TextInput key={i} style={styles.guessSquare}
                  maxLength={1}
                  ref={(input) => { this.seventhTextInput = input; }}
                  blurOnSubmit={false}
                  returnKeyType = { "next" }
                  onSubmitEditing={() => { (!!this.eighthTextInput) ? this.eighthTextInput.focus() : this.lastTextInput.focus() }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(letter) => handleGuessChange(letter, i)} />
              )
            } else if(inputNum === 7 && currentData.structure.length > 12) {
              return(
                <TextInput key={i} style={styles.guessSquare}
                  maxLength={1}
                  ref={(input) => { this.eighthTextInput = input; }}
                  blurOnSubmit={false}
                  returnKeyType = { "next" }
                  onSubmitEditing={() => { this.ninthTextInput.focus(); }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(letter) => handleGuessChange(letter, i)} />
              )
            } else if(inputNum === 8) {
              return(
                <TextInput key={i} style={styles.guessSquare}
                  maxLength={1}
                  ref={(input) => { this.ninthTextInput = input; }}
                  blurOnSubmit={false}
                  returnKeyType = { "next" }
                  onSubmitEditing={() => { this.lastTextInput.focus(); }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(letter) => handleGuessChange(letter, i)} />
              )
            } else {
              return(
                <TextInput key={i} style={styles.guessSquare}
                  maxLength={1}
                  ref={(input) => { this.lastTextInput = input; }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(letter) => handleGuessChange(letter, i)} />
              )
            }
          }
        })}
        {currentData.answer.toLowerCase() === 'հանդուրժողական' && currentData.structure.map((item, i) => {
          if(!!item) {
            return <Text key={i} style={[styles.guessSquare, styles.guessSquareLetter]}>{item}</Text>
          } else {
            inputNum++;

            if(inputNum === 0) {
              return(
                <TextInput key={i} style={styles.guessSquare}
                  maxLength={1}
                  ref={(input) => { this.firstTextInputhand = input; }}
                  blurOnSubmit={false}
                  returnKeyType = { "next" }
                  onSubmitEditing={() => { this.secondTextInput.focus(); }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(letter) => handleGuessChange(letter, i)} />
              )
            } else if(inputNum === 1) {
              return(
                <TextInput key={i} style={styles.guessSquare}
                  maxLength={1}
                  ref={(input) => { this.secondTextInputhand = input; }}
                  blurOnSubmit={false}
                  returnKeyType = { "next" }
                  onSubmitEditing={() => { this.thirdTextInputhand.focus(); }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(letter) => handleGuessChange(letter, i)} />
              )
            } else if(inputNum === 2) {
              return(
                <TextInput key={i} style={styles.guessSquare}
                  maxLength={1}
                  ref={(input) => { this.thirdTextInputhand = input; }}
                  blurOnSubmit={false}
                  returnKeyType = { "next" }
                  onSubmitEditing={() => { this.fourthTextInputhand.focus(); }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(letter) => handleGuessChange(letter, i)} />
              )
            } else if(inputNum === 3) {
              return(
                <TextInput key={i} style={styles.guessSquare}
                  maxLength={1}
                  ref={(input) => { this.fourthTextInputhand = input; }}
                  blurOnSubmit={false}
                  returnKeyType = { "next" }
                  onSubmitEditing={() => { this.fifthTextInputhand.focus(); }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(letter) => handleGuessChange(letter, i)} />
              )
            } else if(inputNum === 4) {
              return(
                <TextInput key={i} style={styles.guessSquare}
                  maxLength={1}
                  ref={(input) => { this.fifthTextInputhand = input; }}
                  blurOnSubmit={false}
                  returnKeyType = { "next" }
                  onSubmitEditing={() => { this.sixthTextInputhand.focus(); }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(letter) => handleGuessChange(letter, i)} />
              )
            } else if(inputNum === 5) {
              return(
                <TextInput key={i} style={styles.guessSquare}
                  maxLength={1}
                  ref={(input) => { this.sixthTextInputhand = input; }}
                  blurOnSubmit={false}
                  returnKeyType = { "next" }
                  onSubmitEditing={() => { this.seventhTextInputhand.focus(); }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(letter) => handleGuessChange(letter, i)} />
              )
            } else if(inputNum === 6) {
              return(
                <TextInput key={i} style={styles.guessSquare}
                  maxLength={1}
                  ref={(input) => { this.seventhTextInputhand = input; }}
                  blurOnSubmit={false}
                  returnKeyType = { "next" }
                  onSubmitEditing={() => { (!!this.eighthTextInputhand) ? this.eighthTextInputhand.focus() : this.lastTextInputhand.focus() }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(letter) => handleGuessChange(letter, i)} />
              )
            } else if(inputNum === 7) {
              return(
                <TextInput key={i} style={styles.guessSquare}
                  maxLength={1}
                  ref={(input) => { this.eighthTextInputhand = input; }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(letter) => handleGuessChange(letter, i)} />
              )
            }
          }
        })}
      </View>
    )
  }
}

const styles = {
  guessSquare: {
    width: 20,
    height: 20,
    marginRight: 2,
    textAlign: 'center',
    borderBottomWidth: 1,
    color: '#61B046',
    borderColor: '#61B046'
  },
  guessSquareLetter: {
    color: '#61B046',
    fontFamily: 'DejavuSans_bold',
    borderBottomWidth: 0
  }
}

export default GuessWord;
