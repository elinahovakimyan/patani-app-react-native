import React from 'react';
import { Text, View, ScrollView, TextInput, Image,TouchableHighlight, TouchableOpacity, Dimensions } from 'react-native';
import { Card, Button, CheckBox, Body, ListItem } from 'native-base';
import { connect } from 'react-redux';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import Modal from "react-native-modal";

import data from '../../../data/game.json';
import Hint from './Hint';
import GuessWord from './GuessWord';
import Puzzle from './Puzzle';
import FindWords from './FindWords';
import FormWord from './FormWord';
import Taraxax from './Taraxax';
import FinishLevel from './FinishLevel';
import GameFooter from './GameFooter';

class StartLevel extends React.Component {
  state = {
    index: 0,
    checked: false,
    checkedItems: [],
    word: '',
    radioSelected: null,
    availablePoints: 0,
    isModalVisible: false,
    crosswordBoard: [],
    formWordSelected: [],
    formWordBase: [],
    puzzleAnswer: false,
    checkedItems: [],
  }

  componentDidMount() {
    const { navigation, levelOverview } = this.props;
    const { level } = navigation.state.params;
    const currentData = data[level][this.state.index];

    this.props.setCurrentNav(navigation);
    this.props.setCurrentQuestion(currentData);
    this.setState({index: levelOverview[level].answers.length});
  }

  componentDidUpdate() {
    const { navigation } = this.props;
    const currentData = data[navigation.state.params.level][this.state.index];

    this.props.setCurrentQuestion(currentData);
    if(!!currentData
      && (currentData.type === 'form-word'
      || currentData.type === 'form-sentence'
      || currentData.type === 'guess-word')
      && this.state.formWordBase.length === 0
      && this.state.formWordSelected.length === 0) {
        this.setState({formWordBase: currentData.partsInArray});
    }
  }

  areEqualArrays = (arr1, arr2) => {
    let isCorrect = false;

    arr1.forEach((item, i) => {
      item.forEach((letter, n) => {
        if(arr1[i][n].toLowerCase() === arr2[i][n].toLowerCase()) {
          isCorrect = true;
        } else {
          isCorrect = false;
        }
      })
    })

    return isCorrect;
  }

  countPoints = () => {
    const { index, checkedItems, word, crosswordBoard, formWordSelected, formWordBase } = this.state;
    const { level } = this.props.navigation.state.params;
    const currentData = data[level][index];
    var currentPoints = this.props.levelOverview;
    var correctAnswer, gainedPoints;

    if(currentData.type === 'radio' && checkedItems.length > 0) {
      (checkedItems[0].isTrue === true) ? correctAnswer = true : correctAnswer = false;
    };

    if(currentData.type === 'radio-image' && checkedItems.length > 0) {
      (checkedItems[0] === currentData.answer) ? correctAnswer = true : correctAnswer = false;
    };

    if(currentData.type === 'image-points' || currentData.type === 'taraxax') {
      let newArr = [];
      for (var i = 0; i < checkedItems.length; i++) {
        if (currentData.answers.includes(checkedItems[i].toLowerCase()) && !newArr.includes(checkedItems[i])) {
          newArr.push(checkedItems[i])
        } else {
          correctAnswer = false;
          break;
        }
      }

      let thirdLength = currentData.answers.length/3;

      if(newArr.length > 2*thirdLength || newArr.length === currentData.answers.length) {
        correctAnswer = true;
      } else if(newArr.length > 0 && newArr.length < thirdLength) {
        correctAnswer = 1;
      } else if(newArr.length >= thirdLength && newArr.length < 2*thirdLength) {
        correctAnswer = 2;
      }
    };

    if(currentData.type === 'checkbox' || currentData.type === 'find-words') {
      var numCorrectAnswers = 0, checkedCorrectAnswers = 0;
      currentData.options.map(option => {
        if(option.isTrue === true) { numCorrectAnswers++; }
      });
      checkedItems.map(item => {
        if(item.isTrue === true && correctAnswer !== false) checkedCorrectAnswers++;
        else if(item.isTrue === false) correctAnswer = false;
      });

      let thirdLength = numCorrectAnswers/3;

      if(checkedCorrectAnswers > 2*thirdLength || checkedCorrectAnswers === numCorrectAnswers) {
        correctAnswer = true;
      } else if(checkedCorrectAnswers > 0 && checkedCorrectAnswers < thirdLength) {
        correctAnswer = 1;
      } else if(checkedCorrectAnswers >= thirdLength && checkedCorrectAnswers < 2*thirdLength) {
        correctAnswer = 2;
      }
    };

    if(currentData.type === 'form-word' || currentData.type === 'form-sentence') {
      let a = (currentData.type === 'form-word') ? '' : ' ';
      correctAnswer = formWordSelected.join(a) === currentData.answer ? true : false;
    }

    if(currentData.type === 'guess-word') {
      formWordBase.join('').toUpperCase() === currentData.answer.toUpperCase() ? correctAnswer = true : correctAnswer = false;
    }

    if(currentData.type === 'crossword') {
      correctAnswer = this.areEqualArrays(crosswordBoard, currentData.board);
    }

    if(currentData.type === 'puzzle') {
      correctAnswer = this.state.puzzleAnswer;
    }

    if(typeof correctAnswer === "number") {
      gainedPoints = correctAnswer
    } else {
      (correctAnswer !== true) ? gainedPoints = 0 : (!!currentPoints[level].hintOpened ? gainedPoints = 1 : gainedPoints = 3);
    }

    let newAnswers = currentPoints[level].answers.filter((item) => item.id !== currentData.id);
    currentPoints[level].answers = [
      ...newAnswers,
      { id: currentData.id, answer: correctAnswer, gainedPoints }
    ];

    this.setState({ gainedPoints, isModalVisible: true });

    this.timerHandle = setTimeout(() => {
      this.setState({isModalVisible: false});
      this.handleNext();
      this.timerHandle = 0;
    }, 1000);

    currentPoints[level].points += gainedPoints;
    currentPoints[level].hintOpened = false;
    this.props.updatePoints(currentPoints);
  }

  componentWillUnmount() {
    if (this.timerHandle) {
      clearTimeout(this.timerHandle);
      this.timerHandle = 0;
    }
  }

  handleNext = () => {
    const { level } = this.props.navigation.state.params;
    const currentData = data[level][this.state.index];
    var currentPoints = this.props.levelOverview;

    this.setState({
      checkedItems: [],
      formWordBase: [],
      formWordSelected: [],
      index: this.state.index + 1,
      radioSelected: null,
    });
  }

  handleRadioSelect = (index, value) => {
    this.setState({
      checkedItems: [value],
      radioSelected: index
    });
  }

  handleCheckboxSelect = (option) => {
    const { checkedItems } = this.state;

    if(!checkedItems.includes(option)) {
      this.setState({ checkedItems: [...this.state.checkedItems, option] });
    } else {
      const newArr = checkedItems.filter(item => item !== option)
      this.setState({ checkedItems: newArr });
    }
  }

  handleFormWordChange = (newState) => {
    this.setState(newState)
  }

  handleCrosswordChange = (letter, row, field) => {
    const { level } = this.props.navigation.state.params;
    const { index } = this.state;
    const currentData = data[level][index];
    const { structure } = currentData;

    structure[row][field] = letter;
    this.setState({crosswordBoard: structure});
  }

  handleFindWordsChange = (option, action) => {
    if(action === 'add') {
      this.setState({ checkedItems: [...this.state.checkedItems, option] });
    } else {
      this.setState({ checkedItems: this.state.checkedItems.filter(x => x !== option)});
    }
  }

  handleTaraxaxChange = (answers) => {
    this.setState({checkedItems: answers})
  }

  handlePuzzleChange = (isTrue) => {
    this.setState({puzzleAnswer: isTrue})
  }

  handleGuessChange = (letter, i) => {
    const { level } = this.props.navigation.state.params;
    const { index, formWordSelected, formWordBase } = this.state;
    const currentData = data[level][index];

    formWordBase[i] = letter;
    this.setState({formWordBase});
  }

  onTouchEvent(name, ev) {
    const { checkedItems } = this.state;

    if((ev.nativeEvent.locationX > 115 && ev.nativeEvent.locationX < 155 &&
       ev.nativeEvent.locationY > 78 && ev.nativeEvent.locationY < 115)
       || (ev.nativeEvent.locationX > 5 && ev.nativeEvent.locationX < 55 &&
          ev.nativeEvent.locationY > 78 && ev.nativeEvent.locationY < 115)
    ) {
      if(!checkedItems.includes('ձեռքեր')) {
        this.setState((prevState) => ({
          checkedItems: [...prevState.checkedItems, 'ձեռքեր']
        }));
      }
    }

    if((ev.nativeEvent.locationX > 65 && ev.nativeEvent.locationX < 80 &&
       ev.nativeEvent.locationY > 190 && ev.nativeEvent.locationY < 245)
       || (ev.nativeEvent.locationX > 90 && ev.nativeEvent.locationX < 105 &&
          ev.nativeEvent.locationY > 190 && ev.nativeEvent.locationY < 245)
    ) {
      if(!checkedItems.includes('ոտքեր')) {
        this.setState((prevState) => ({
          checkedItems: [...prevState.checkedItems, 'ոտքեր']
        }));
      }
    }

    if(ev.nativeEvent.locationX > 70 && ev.nativeEvent.locationX < 100 &&
       ev.nativeEvent.locationY > 85 && ev.nativeEvent.locationY < 110) {
      if(!checkedItems.includes('կրծքավանդակ')) {
        this.setState((prevState) => ({
          checkedItems: [...prevState.checkedItems, 'կրծքավանդակ']
        }));
      }
    }

    if(ev.nativeEvent.locationX > 75 && ev.nativeEvent.locationX < 90 &&
       ev.nativeEvent.locationY > 52 && ev.nativeEvent.locationY < 62) {
      if(!checkedItems.includes('վերին ծնոտ')) {
        this.setState((prevState) => ({
          checkedItems: [...prevState.checkedItems, 'վերին ծնոտ']
        }));
      }
    }

    if(ev.nativeEvent.locationX > 80 && ev.nativeEvent.locationX < 92 &&
       ev.nativeEvent.locationY > 145 && ev.nativeEvent.locationY < 158) {
      if(!checkedItems.includes('սեռական օրգաններ')) {
        this.setState((prevState) => ({
          checkedItems: [...prevState.checkedItems, 'սեռական օրգաններ']
        }));
      }
    }
  }

  handleBack = () => {
    const { index } = this.state;
    const { levelOverview, updatePoints, navigation } = this.props;
    const { level } = navigation.state.params;

    const answered = levelOverview[level].answers.length;
    const removePoints = levelOverview[level].answers[answered - 1].gainedPoints;
    levelOverview[level].points += -removePoints;
    levelOverview[level].answers.pop();
    updatePoints(levelOverview);

    if(index > 0) {
      this.setState((prevState) => ({
        index: prevState.index - 1
      }));
    }
  }

  render() {
    const { navigation, levelOverview } = this.props;
    const { level } = navigation.state.params;
    const { index, checkedItems, radioSelected, formWordSelected, gainedPoints, formWordBase } = this.state;
    const currentData = data[level][index];
    let inputNum = 0;

    if(index < 10) {
      return(
        <Card style={styles.wrapper}>

          {!!currentData &&
          <ScrollView style={{marginBottom: 32}}>
            {!!currentData.question && <Text style={styles.question}>{currentData.id}. {currentData.question}</Text>}

            {currentData.type === 'radio' &&
              <RadioGroup size={18} thickness={2} color='#61B046' selectedIndex={radioSelected}
                onSelect = {(index, value) => this.handleRadioSelect(index, value)}>
                {currentData.options.map((option, i) => {
                  return(
                    <RadioButton style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}} key={option.label}
                      value={option} color={(i === radioSelected) ? '#61B046' : '#5e5f5e'}>
                      {i === radioSelected &&
                        <Text style={{color: '#61B046', fontFamily: 'DejavuSans_bold'}}>{option.label}</Text>
                      }
                      {i !== radioSelected &&
                        <Text style={{color: '#5e5f5e', fontFamily: 'DejavuSans'}}>{option.label}</Text>
                      }
                    </RadioButton>
                  )
                })}
              </RadioGroup>}


            {currentData.type === 'radio-image' &&
              <RadioGroup 
                size={18}
                thickness={2}
                color='#61B046'
                selectedIndex={radioSelected}
                onSelect = {(index, value) => this.handleRadioSelect(index, value)}
              >
                <RadioButton value='bully' color='#61B046' style={styles.radioImgWrap}>
                  <Image source={require('../../../assets/game/ic_bully_xhdpi.png')} style={styles.radioImg}/>
                </RadioButton>
                <RadioButton value='doctor' color='#61B046' style={styles.radioImgWrap}>
                  <Image source={require('../../../assets/game/ic_doctor_xhdpi.png')} style={styles.radioImg}/>
                </RadioButton>
                <RadioButton value='cry' color='#61B046' style={styles.radioImgWrap}>
                  <Image source={require('../../../assets/game/ic_cry_xhdpi.png')} style={styles.radioImg}/>
                </RadioButton>
              </RadioGroup>}


              {currentData.type === 'checkbox' &&
                currentData.options.map((option) => {
                  let isChecked;
                  checkedItems.includes(option) ? isChecked = true : isChecked = false;
                  return(
                    <ListItem style={{borderBottomWidth:0, paddingBottom:4, paddingTop:4}} key={option.label}>
                      <CheckBox checked={isChecked}
                        color={!!isChecked ? '#61B046' : '#5e5f5e'}
                        onPress={() => this.handleCheckboxSelect(option)}
                        />
                      {!!isChecked &&
                        <Text style={{marginLeft:10,marginBottom: 4,marginRight:3,color:'#61B046',fontFamily:'DejavuSans_bold' }}>
                          {option.label}
                        </Text>
                      }
                      {!isChecked &&
                        <Text style={{marginLeft:10,marginBottom: 4,marginRight:3,color: '#5e5f5e',fontFamily:'DejavuSans'}}>{option.label}</Text>
                      }
                    </ListItem>
                  )
                })}

                {(currentData.type === 'form-word' || currentData.type === 'form-sentence') &&
                  <FormWord handleChange={this.handleFormWordChange}
                    formWordSelected={formWordSelected}
                    formWordBase={formWordBase}
                    currentData={currentData}
                  />
                }

                {currentData.type === 'puzzle' &&
                  <Puzzle onPuzzleChange={this.handlePuzzleChange} currentData={currentData} />
                }

                {currentData.type === 'image-points' &&
                  <View style={{display: 'flex', flexDirection: 'row'}}>
                    <View
                      onStartShouldSetResponder={(ev) => true}
                      onMoveShouldSetResponder={(ev) => true}
                      onResponderGrant={this.onTouchEvent.bind(this, "onResponderGrant")}
                      onResponderMove={this.onTouchEvent.bind(this, "onResponderMove")}
                      style={{width: 170, height:270}}
                    >
                      <Image
                        style={{width: 170, height:270}}
                        source={require('../../../assets/game/ic_boy_xhdpi.png')}
                      />
                    </View>
                    <View>
                      {checkedItems.length > 0 &&
                        checkedItems.map(name => <Text key={name} style={styles.imgPartsTxt}>• {name}</Text>)
                      }
                    </View>
                  </View>
                }



                {currentData.type === 'find-words' &&
                  <View>
                    {currentData.options.map((option) => (
                      <FindWords key={option.label} onFindWordsChange={this.handleFindWordsChange} option={option}/>
                    ))}
                  </View>
                }

                {currentData.type === 'taraxax' &&
                  <Taraxax currentData={currentData} handleTaraxaxChange={this.handleTaraxaxChange}/>
                }

                {currentData.type === 'crossword' &&
                <ScrollView>
                  {currentData.board.map((row, rowIndex) => {
                    return(
                      <View style={{flexDirection: 'row'}} key={rowIndex}>
                        {row.map((field, fieldIndex) => {
                          if(field === 0) {
                            return <Text key={fieldIndex} style={styles.crosswordSquare}></Text>
                          } else if (typeof field === 'number') {
                            return <Text key={fieldIndex} style={[styles.crosswordSquare, {fontSize: 8, color: '#5e5f5e'}]}>{field}</Text>
                          } else {
                            return(
                              <TextInput key={fieldIndex}
                                  style={[styles.crosswordSquare, {borderWidth: 0.5, borderColor: '#5e5f5e'}]}
                                  autoCapitalize='none'
                                  underlineColorAndroid={'rgba(0,0,0,0)'}
                                  maxLength={(field.toLowerCase() === 'ու') ? 2 : 1}
                                  onChangeText={(letter) => this.handleCrosswordChange(letter, rowIndex, fieldIndex)}
                              />
                            )
                          }
                        })}
                      </View>
                    )
                  })}
                  {currentData.questionList.map(question => {
                    if(typeof question === 'string') {
                      return(
                        <Text key={question} style={{fontFamily: 'DejavuSans', color: '#5e5f5e', fontSize: 12}}>{question}</Text>
                      )
                    } else {
                      return(
                        <Text key={question.label} style={{fontFamily: 'DejavuSans_bold_oblique', color: '#5e5f5e', fontSize: 12}}>{question.label}</Text>
                      )
                    }
                  })}
                </ScrollView>
                }

                {currentData.type === 'guess-word' &&
                  <GuessWord handleGuessChange={this.handleGuessChange} currentData={currentData} />
                }

              </ScrollView>
            }

            <GameFooter
              index={index}
              level={level}
              levelOverview={levelOverview}
              onSubmit={this.countPoints}
              onBack={this.handleBack}
            />

            <Modal isVisible={this.state.isModalVisible} style={styles.modalWrapper} animationIn='bounceIn' animationInTiming={300} animationOut='bounceOut'>
              <View style={styles.modalChildStyle}>
                {gainedPoints === 3 &&
                  <Image style={styles.modalImg} source={require('../../../assets/game/ic_3stars_xhdpi.png')} />
                }
                {gainedPoints === 2 &&
                  <Image style={styles.modalImg} source={require('../../../assets/game/ic_2stars_xhdpi.png')} />
                }
                {gainedPoints === 1 &&
                  <Image style={styles.modalImg} source={require('../../../assets/game/ic_1star_xhdpi.png')} />
                }
                {gainedPoints === 0 &&
                  <Image style={styles.modalImg} source={require('../../../assets/game/ic_0_points_xhdpi.png')} />
                }
              </View>
            </Modal>
          </Card>
        )
      } else {
        return <FinishLevel points={levelOverview[level].points} level={level} navigation={navigation}/>
      }
  }
}

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const styles = {
  wrapper: {
    marginRight: 10,
    marginLeft: 10,
    marginTop: 10,
    marginBottom: 12,
    padding: 12,
    borderRadius: 10,
    flex: 1
  },
  gameImg: {
    width: 180,
    height: 220
  },
  question: {
    marginBottom: 10,
    color: '#5e5f5e',
    fontSize: 16,
    fontFamily: 'DejavuSans_bold'
  },
  crosswordSquare: {
    width: 17.8,
    height: 20,
    textAlign: 'center'
  },
  taraxaxSquare: {
    width: 19,
    height: 35,
    textAlign: 'center'
  },
  imgPartsTxt: {
    fontFamily: 'DejavuSans',
    color: '#61B046',
    fontSize: 12
  },
  modalWrapper: {
    borderRadius: 8,
    width,
    height,
    margin: 0,
  },
  modalChildStyle: {
    paddingStart: 6,
    backgroundColor: 'rgba(52, 52, 52, 0.2)'
  },
  modalImg: {
    width: width-11,
    height
  },
  modalContentText: {
    textAlign: 'center',
  },
  radioImg: {
    width: width - 150,
    height: 180,
  },
  radioImgWrap: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  closeBtnImg: {
    width: 20,
    height: 20
  },
  closeBtn: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  }
}

function mapStateToProps(state) {
  return {
    levelOverview: state.gameReducers.levelOverview
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setCurrentNav: (nav) => dispatch({ type: 'SET_CURRENT_NAV', nav }),
    setCurrentQuestion: (question) => dispatch({ type: 'SET_CURRENT_QUESTION', question }),
    updatePoints: (levelOverview) => dispatch({ type: 'UPDATE_POINTS', levelOverview }),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(StartLevel);
