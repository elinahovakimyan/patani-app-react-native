import React, { Component } from 'react';
import { Image, Dimensions, ScrollView } from 'react-native';
import { Text, Card } from 'native-base';
import { connect } from 'react-redux';
import HTML from 'react-native-render-html';
import data from '../../../data/content.json';

class SingleMain extends Component {
  static navigationOptions = {
    title: 'ContentItem'
  }

  state = {
    currentData: {}
  }

  componentWillMount() {
    const { navigation } = this.props;
    const { title, id } = navigation.state.params;
    const currentData = data.filter((item) => item.id === id );
    this.setState({currentData: currentData[0]});

    this.props.setCurrentNav(navigation);
  }

  render() {
    const { currentData } = this.state;
    const { navigation } = this.props;
    const { key, title } = navigation.state.params;
    const specData = (!!currentData) ? currentData[`for_${key.toLowerCase()}`] : {};

    return (
      <Card style={styles.container}>
        {!!currentData &&
        <ScrollView>

          <HTML html={currentData.content} baseFontStyle={{fontFamily: 'DejavuSans'}} />

          {!!specData &&
            <HTML html={currentData[`for_${key.toLowerCase()}`]} baseFontStyle={{fontFamily: 'DejavuSans'}} />
          }

        </ScrollView>}

        {!currentData && <Text style={{textAlign: 'center'}}>Սպասիր․․․</Text>}
      </Card>
    );
  }
}

const width = Dimensions.get('window').width; //full width

const styles = {
  container: {
    marginTop: 8,
    marginBottom: 8,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 10,
    padding: 10,
    backgroundColor: 'white'
  },
  higImg: {
    width: 280,
    height: 250,
    margin: 10,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  brnImg: {
    width: 280,
    height: 180,
    margin: 10,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  textStyle: {
    fontFamily: 'DejavuSans',
    fontSize: '15dp',
    color: '#5e5f5e'
  }
}


function mapDispatchToProps(dispatch) {
  return {
    setCurrentNav: (nav) => dispatch({ type: 'SET_CURRENT_NAV', nav }),
  }
}

export default connect(null, mapDispatchToProps)(SingleMain);
