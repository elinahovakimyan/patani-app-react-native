import React, { Component } from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body } from 'native-base';
import data from '../../../data/content.json';

class ListItem extends Component {
  constructor(props) {
    super(props);
    this.getImgSrc = this.getImgSrc.bind(this)
  }

  getImgSrc() {
    const { imgSrc } = this.props;
    if (imgSrc.includes('ic_ser_xhdpi')) return require('../../../assets/content/ic_ser_xhdpi.png');
    else if (imgSrc.includes('ic_serahasunacum_xhdpi')) return require('../../../assets/content/ic_serahasunacum_xhdpi.png');
    else if (imgSrc.includes('ic_serakan_organner_xhdpi')) return require('../../../assets/content/ic_serakan_organner_xhdpi.png');
    else if (imgSrc.includes('ic_higiena_xhdpi')) return require('../../../assets/content/ic_higiena_xhdpi.png');
    else if (imgSrc.includes('ic_hghiutyun_xhdpi')) return require('../../../assets/content/ic_hghiutyun_xhdpi.png');
    else if (imgSrc.includes('ic_marmni_andzernamkheliutyun_xhdpi')) return require('../../../assets/content/ic_marmni_andzernamkheliutyun_xhdpi.png');
  }

  render() {
    const { navigation, title, imgSrc, id } = this.props;
    const { key } = navigation.state;

    return (
        <TouchableOpacity style={{borderRadius: 10}} onPress={() => navigation.navigate('ContentItem', { title, key, id })}>
          <Card style={styles.cardStyle}>
            <CardItem style={styles.cardItem}>
              <Left>
                <Image source={this.getImgSrc()} style={styles.imgStyle}/>
                <Body>
                  <Text style={styles.textStyle}>{title}</Text>
                </Body>
              </Left>
            </CardItem>
        </Card>
      </TouchableOpacity>
    );
  }
}

const styles = {
  cardStyle: {
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 10,
    padding:0,
    margin: 0,
    borderColor: '#e5e5e5',
    shadowOffset: {
      width: 10,
      height: 0.5
    },
    shadowOpacity: 0.1,
    shadowRadius: 10,
    elevation: 2
  },
  cardItem: {
    paddingTop: 4,
    paddingLeft: 4,
    paddingBottom: 4,
    borderRadius: 10,
  },
  imgStyle: {
    width: 60,
    height: 60,
    margin: 0,
    padding: 0
  },
  textStyle: {
    fontFamily: 'DejavuSans_bold',
    fontSize: 15,
    color: '#5e5f5e'
  }
}

export default ListItem;
