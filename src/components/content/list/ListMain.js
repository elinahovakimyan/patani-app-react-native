import React, { Component } from 'react';
import { Content, List } from 'native-base';
import { connect } from 'react-redux';
import ListItem from './ListItem';
import data from '../../../data/content.json';

class ListMain extends Component {
    state = {
      contentData: []
    }

    componentDidMount() {
      const { navigation } = this.props;
      const { state } = navigation;
      if(state.key.toLowerCase() === 'male') {
        const maleData = data.filter(item => item.title.toLowerCase() !== 'հղիություն')
        this.setState({contentData: maleData});
      } else {
        this.setState({contentData: data});
      }
      this.props.setCurrentNav(navigation);

    }

    render () {
      const navigation = this.props.navigation;
      const { contentData } = this.state;

      return (
        <Content style={{backgroundColor: 'white'}}>
          <List>
            {contentData.length > 0 && contentData.map((item, i) => {
              return(
                <ListItem key={i} theme={{fontSize: 16}}
                    navigation={navigation}
                    id={i}
                    title={item.title.toUpperCase()}
                    imgSrc={item.imgName}
                />
              )
            })}
          </List>
        </Content>
      );
    }
}

function mapStateToProps(state) {
  return {
    nav: state.contentReducers.nav,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setCurrentNav: (nav) => dispatch({ type: 'SET_CURRENT_NAV', nav }),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListMain);
