import React from 'react';
import { Image, TouchableOpacity, AsyncStorage } from 'react-native';
import { Text, Grid, Col } from 'native-base';
import { StackNavigator } from 'react-navigation';
import { connect } from 'react-redux';

class InitialChoice extends React.Component {
  static navigationOptions = {
    title: 'Home'
  }

  async componentDidMount() {
    try {
      const { navigation } = this.props;
      await this.props.setCurrentNav(navigation);

      const value = await AsyncStorage.getItem('@MySuperStore:gameData');
      if(value !== null) {
        this.props.updatePoints(JSON.parse(value));
      }
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    const { navigate } = this.props.navigation;

    return(
      <Grid>
        <Col style={[styles.column, {marginLeft: 7}]}>
          <TouchableOpacity onPress={() => navigate('ReadInstructions')} style={{elevation: 3}}>
            <Image source={require('../../assets/kardal.png')} style={styles.img}/>
          </TouchableOpacity>
        </Col>
        <Col style={[styles.column, {marginRight: 7}]}>
          <TouchableOpacity onPress={() => navigate('GameInstructions')} style={{elevation: 1}}>
            <Image source={require('../../assets/xaxal.png')} style={styles.img}/>
          </TouchableOpacity>
        </Col>
      </Grid>
    )
  }
}

const styles = {
  column: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  img: {
    // width: 198,
    // height: 80,
  }
};


function mapStateToProps(state) {
  return {
    levelOverview: state.gameReducers.levelOverview
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setCurrentNav: (nav) => dispatch({ type: 'SET_CURRENT_NAV', nav }),
    updatePoints: (levelOverview) => dispatch({ type: 'UPDATE_POINTS', levelOverview }),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(InitialChoice);
