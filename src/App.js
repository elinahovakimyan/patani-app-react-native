import React from 'react';
import { AppRegistry, View, AsyncStorage, Image, Dimensions } from 'react-native';
import { Container } from 'native-base';
import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import { CustomDrawer, CustomDrawerContent, CustomDrawerGame } from './navigation/MainNavigators';
import Header from './components/common/Header';

class App extends React.Component {
  state = {
    isReady: false
  };

  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this._cacheResourcesAsync}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
      );
    }

    return (
      <Container style={{backgroundColor: 'white'}}>
        <View style={styles.mainHeader}/>
        <Header />
        <CustomDrawer />
      </Container>
    );
  }

  async _cacheResourcesAsync() {
    const images = [
      require('./assets/kardal.png'),
      require('./assets/xaxal.png'),
      require('./assets/ic_play_1_xhdpi.png'),
      require('./assets/ic_play_2_xhdpi.png'),
      require('./assets/ic_play_3_xhdpi.png'),
      require('./assets/ic_read_xhdpi.png'),
      require('./assets/game/ic_3stars_xhdpi.png'),
      require('./assets/game/ic_2stars_xhdpi.png'),
      require('./assets/game/ic_1star_xhdpi.png'),
      require('./assets/game/ic_0_points_xhdpi.png'),
      require('./assets/game/ic_not_enough_points_xhdpi.png'),
    ];

    const cacheImages = images.map((image) => {
      return Asset.fromModule(image).downloadAsync();
    });
    return Promise.all(cacheImages)

  }
}

const width = Dimensions.get('window').width; //full width
const height = Dimensions.get('window').height; //full width

const styles = {
  mainHeader: {
    backgroundColor: '#61B046',
    height: 25
  },
  launchIcon: {
    width: width,
    height: height,
    backgroundColor: 'red',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ededed',
  }
}

export default App
